import os
import pytest
import contextlib
import shutil
import pkg_resources
from pprint import pprint
from git import Repo, Git, BadName
from lb.nightly.configuration import Project, Package, DataProject
from lb.nightly.functions import checkout


@contextlib.contextmanager
def working_directory(path):
    """A context manager which changes the working directory to the given
    path, and then changes it back to its previous value on exit.

    """
    prev_cwd = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(prev_cwd)


def test_git_entry_point():
    git_checkout = next(
        pkg_resources.iter_entry_points("lb.nightly.checkout_methods", "git")
    ).load()
    assert git_checkout is checkout.git


@pytest.mark.parametrize(
    "project",
    (
        Project("Gaudi", "master"),
        Project("Lbcom", "HEAD"),
        Package("PRConfig", "master"),
        DataProject("DBASE", packages=[Package("PRConfig", "master")]),
    ),
)
def test_git_basic(tmp_path, project):
    with working_directory(tmp_path):
        report = checkout.git(project)

        if isinstance(project, DataProject):
            assert hasattr(report, "packages")
            report = report.packages[0]

        assert os.path.isdir(project.name)
        assert os.listdir(project.name)
        assert report.gitlab_id == {
            "Gaudi": "gaudi/Gaudi",
            "Lbcom": "lhcb/Lbcom",
            "PRConfig": "lhcb-datapkg/PRConfig",
            "DBASE": "lhcb-datapkg/PRConfig",
        }[project.name]

        # check report metadata
        repo_dir = (
            project.name
            if not isinstance(project, DataProject)
            else os.path.join(
                project.name, project.packages[0].name, project.packages[0].version
            )
        )
        repo = Repo(repo_dir)
        assert report.commit == repo.head.commit.hexsha
        assert f"commit: {report.commit}" in str(report)
        assert report.tree == repo.head.commit.tree.hexsha
        assert f"tree: {report.tree}" in str(report)

        # make sure the repository is clean
        Git(repo_dir).diff(quiet=True, exit_code=True)


def test_git_bad_arg():
    with pytest.raises(TypeError):
        checkout.git("dummy")


def test_git_bad_ref():
    with pytest.raises(ValueError):
        checkout.git(Project("Gaudi", "dummy"))


def test_git_bad_commit():
    with pytest.raises(BadName):
        checkout.git(Project("Gaudi", "master", checkout_opts={"commit": "00000000"}))


def test_git_existing_dir(tmp_path):
    with working_directory(tmp_path):
        repo = Repo.clone_from("https://gitlab.cern.ch/gaudi/Gaudi.git", "Gaudi")
        # fake commit to make sure we do not use the origin/master
        repo.index.commit("test")
        expected_tree = repo.head.commit.tree.hexsha
        report = checkout.git(Project("Gaudi", "master"))
        assert report.tree == expected_tree
        assert not repo.is_dirty()


def test_git_custom_url(tmp_path):
    with working_directory(tmp_path):
        # dummy source repo
        repo = Repo.init("Gaudi-ref")
        repo.index.commit("test")

        expected_tree = repo.head.commit.tree.hexsha
        report = checkout.git(
            Project("Gaudi", "master", checkout_opts={"url": repo.working_dir})
        )
        assert report.tree == expected_tree
        assert not Repo("Gaudi").is_dirty()


def test_git_bad_submodule(tmp_path):
    with working_directory(tmp_path):
        # prepare submodule source
        dummy = Repo.init("dummy")
        dummy.index.commit("stuff")
        # prepare fake repo
        ref = Repo.init("Project-ref")
        ref.create_submodule("dummy", "dummy", url=dummy.working_dir)
        ref.index.commit("first")
        expected_tree = ref.head.commit.tree.hexsha
        # drop dummy
        shutil.rmtree(dummy.working_dir)
        # checkout
        report = checkout.git(
            Project("Gaudi", "master", checkout_opts={"url": ref.working_dir})
        )
        output = str(report)
        print(output)
        assert "dummy: warning: failed to get submodule:" in output
        assert report.submodules["failure"] == ["dummy"]
        assert report.tree == expected_tree
        assert Repo("Gaudi").is_dirty()


@pytest.mark.parametrize(
    "project,expected_tree",
    [
        (
            Project(
                "Lbcom",
                "HEAD",
                checkout_opts={"commit": "f9713421fd174f0da62a07dce6db052d91eb362c"},
            ),
            "fab2ae6197984468b72b69cf342b89cf2e8d3a85",
        ),
        (
            Project(
                "Gaudi",
                "HEAD",
                checkout_opts={
                    "commit": "770980137ead8585434bd53045653379682a5069",
                    "merges": [
                        (942, "af50c136494391267095b1187fa2a78773889720"),
                        (1063, "f68469bd9764ecd5a1d46f89c2a565ac3d9215b6"),
                    ],
                },
            ),
            "ffed7bfa724e02fd3cd1d64ddfc12000d7135b40",
        ),
        (
            Project(
                "LHCb",
                "master",
                checkout_opts={"commit": "adc8e751549597755d185dcd0917789b90226e61"},
            ),
            "60abd146ca35bf1247a861608fcd113b5c240722",
        ),
        (
            Package(
                "PRConfig",
                "master",
                checkout_opts={"commit": "945d95bfb2cf3ae3eb6c969cd1791be7ac532a36"},
            ),
            "2528a6c56a944d2be3cb4be5298bb6e21d2a31b2",
        ),
    ],
)
def test_git_reproducible_trees(tmp_path, project, expected_tree):
    pprint(project.toDict())
    with working_directory(tmp_path):
        report = checkout.git(project)
        assert os.path.isdir(project.name)
        assert os.listdir(project.name)
        assert report.tree == expected_tree
        assert not Repo(project.name).is_dirty()


def test_git_merge_failure(tmp_path):
    project = Project(
        "Kepler",
        "HEAD",
        checkout_opts={
            "commit": "34fc0264ae3abd64f3adc99f3748ff718b68a8ff",
            "merges": [
                (35, "2594aa3febdcf1bc0f5628a5e403871c74beaca4"),
                (44, "bbf738d9decb14c32fbeb79d160f8814861bf6f4"),
                (0, "0000000000000000000000000000000000000000"),
            ],
        },
    )
    expected_tree = "dabaab89f2a559cdd29f98be20f1687069ad4849"
    pprint(project.toDict())
    with working_directory(tmp_path):
        report = checkout.git(project)
        output = str(report)
        print(output)
        assert os.path.isdir(project.name)
        assert os.listdir(project.name)
        assert "warning: failed to merge lhcb/Kepler!44" in output
        assert "warning: failed to merge lhcb/Kepler!0" in output
        assert report.merges["success"] == [35]
        assert report.merges["failure"] == [44, 0]
        assert report.tree == expected_tree
        assert not Repo(project.name).is_dirty()


def test_non_default_branch(tmp_path):
    project = Project(
        "Detector",
        "v0-patches",
    )
    pprint(project.toDict())
    with working_directory(tmp_path):
        report = checkout.git(project)
        output = str(report)
        print(output)
        assert os.path.isdir(project.name)
        assert os.listdir(project.name)
        assert not Repo(project.name).is_dirty()


def test_tag(tmp_path):
    project = Project(
        "Detector",
        "v0r4",
    )
    pprint(project.toDict())
    with working_directory(tmp_path):
        report = checkout.git(project)
        output = str(report)
        print(output)
        assert os.path.isdir(project.name)
        assert os.listdir(project.name)
        assert not Repo(project.name).is_dirty()
