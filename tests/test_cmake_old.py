import os
from unittest.mock import patch
from lb.nightly.configuration import Project
from lb.nightly.functions.build import cmake_old


@patch(
    "lb.nightly.functions.build.run",
)
@patch(
    "lb.nightly.functions.build.singularity_run",
)
def test_build(singularity_run, run, caplog):
    singularity_run.return_value.returncode = 0
    run.return_value.returncode = 0
    result = cmake_old(Project("Gaudi", "master")).build()
    for target in (
        "configure",
        "all",
        "unsafe-install",
        "post-install",
        "clean",
    ):
        assert f"make BUILDDIR=build {target}" in result[target].stdout
        assert result[target].returncode == 0
