import os
from unittest.mock import patch
from lb.nightly.configuration import Project
from lb.nightly.functions.build import cmt


@patch(
    "lb.nightly.functions.build.run",
)
@patch(
    "lb.nightly.functions.build.singularity_run",
)
def test_build(singularity_run, run, caplog):
    singularity_run.return_value.returncode = 0
    run.return_value.returncode = 0
    os.environ["CMTCONFIG"] = "x86_64-centos7-gcc9-opt"
    os.environ["BINARY_TAG"] = os.environ["CMTCONFIG"]
    result = cmt(Project("Gaudi", "master")).build()
    assert "command exited with code 0" in str(result.records)
    assert "cmt run make all" in result.stdout
