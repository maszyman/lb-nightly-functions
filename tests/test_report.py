###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from lb.nightly.functions.common import Report

import pytest
import logging


@pytest.mark.parametrize("method", ("debug", "info", "warning", "error"))
def test_basic_logging_methods(caplog, method):
    caplog.set_level(logging.DEBUG)

    text = f"{method}: this is a '{method}' message"

    report = Report("test_logger")
    getattr(report, method)(text)

    assert text in caplog.text
    assert report.records == [{"type": method, "level": method, "text": text}]


@pytest.mark.parametrize("method", ("warning", "error"))
def test_add_warn_tag(caplog, method):
    caplog.set_level(logging.DEBUG)

    text = f"this is a '{method}' message"

    report = Report("test_logger")
    getattr(report, method)(text)

    updated_text = f"{method}: {text}"
    assert text in caplog.text
    assert report.records == [{"type": method, "level": method, "text": updated_text}]


def test_log_method(caplog):
    caplog.set_level(logging.DEBUG)

    report = Report("test_logger")
    report.log("some_type", "debug", "a %s message", ("simple",))

    assert "a simple message" in caplog.text
    assert report.records == [
        {"type": "some_type", "level": "debug", "text": "a simple message"}
    ]


def test_git_error_handling():
    from git import GitCommandError

    report = Report("test_logger")
    report.git_error(
        "some failure", GitCommandError(["git", "command"], 128, "error message")
    )

    assert report.records == [
        {
            "type": "warning",
            "level": "warning",
            "text": "warning: some failure: GitCommand status 128",
        },
        {"type": "command", "level": "debug", "text": "> git command"},
        {
            "type": "stderr",
            "level": "debug",
            "text": "error message",
        },
    ]

    report = Report("test_logger")
    report.git_error(
        "some failure",
        GitCommandError(
            ["git", "something", "else"], 1, "error message", "output message\n"
        ),
    )

    assert report.records == [
        {
            "type": "warning",
            "level": "warning",
            "text": "warning: some failure: GitCommand status 1",
        },
        {"type": "command", "level": "debug", "text": "> git something else"},
        {
            "type": "stdout",
            "level": "debug",
            "text": "output message",
        },
        {
            "type": "stderr",
            "level": "debug",
            "text": "error message",
        },
    ]


def test_str():
    report = Report("test_logger")
    report.info("line 1")
    report.debug("line 2")

    expected = "line 1\nline 2"
    assert str(report) == expected

    report = Report("test_logger")
    report.project = {"name": "MyProject", "version": "0.0.0"}
    report.info("line 1")
    report.debug("line 2")
    pkg_report = Report("test_logger")
    pkg_report.debug("line 3")
    report.packages = [pkg_report]

    expected = "MyProject/0.0.0\n---------------\n\nline 1\nline 2\nline 3"
    assert str(report) == expected


def test_md_base():
    report = Report("test_logger")
    report.info("line 1")
    report.debug("line 2")

    expected = "- line 1\n- line 2"
    assert report.md() == expected

    report = Report("test_logger")
    report.project = {"name": "MyProject", "version": "0.0.0"}
    report.info("line 1")
    report.debug("line 2")
    pkg_report = Report("test_logger")
    pkg_report.debug("line 3")
    report.packages = [pkg_report]

    expected = "# MyProject/0.0.0\n- line 1\n- line 2\n- line 3"
    assert report.md() == expected


@pytest.mark.parametrize("special_type", ("stdout", "stderr", "command"))
def test_md_with_stdout(special_type):
    report = Report("test_logger")
    report.info("line 1")
    report.log(special_type, "debug", "line 2")

    expected = "- line 1\n  ```\n  line 2\n  ```"
    assert report.md() == expected


def test_dict():
    assert Report("dummy").to_dict() == {"logger": "dummy", "records": []}

    report = Report("with-member")
    report.a_member = "some value"
    assert report.to_dict() == {
        "logger": "with-member",
        "records": [],
        "a_member": "some value",
    }

    report = Report("with-record")
    report.info("a message")
    assert report.to_dict() == {
        "logger": "with-record",
        "records": [{"level": "info", "text": "a message", "type": "info"}],
    }

    report = Report("with-packages")
    report.project = {"name": "MyProject", "version": "0.0.0"}
    report.info("a message")
    pkg_report = Report("the-package")
    pkg_report.project = {"name": "SomePackage", "version": "abc"}
    pkg_report.debug("another message")
    report.packages = [pkg_report]

    assert report.to_dict() == {
        "logger": "with-packages",
        "project": {"name": "MyProject", "version": "0.0.0"},
        "records": [{"level": "info", "text": "a message", "type": "info"}],
        "packages": [
            {
                "logger": "the-package",
                "project": {"name": "SomePackage", "version": "abc"},
                "records": [
                    {"level": "debug", "text": "another message", "type": "debug"}
                ],
            }
        ],
    }
