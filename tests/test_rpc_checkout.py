###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
import os
import zipfile
from unittest.mock import patch, MagicMock, Mock


from lb.nightly.functions.rpc import checkout
from lb.nightly.configuration import Slot, Project, DBASE, Package, DataProject

from .test_checkout import working_directory


class MockGit:
    def __init__(self, report=None):
        self.report = Mock() if report is None else report

    def __call__(self, project, *args, **kwargs):
        os.makedirs(project.name)
        with open(os.path.join(project.name, "data.txt"), "w") as f:
            f.write("data\n")
        return self.report


@patch(
    "lb.nightly.functions.rpc.service_config",
    return_value={
        "gitlab": {"token": "some_token"},
        "artifacts": {"uri": "artifacts"},
        "logs": {"uri": "logs"},
    },
)
@patch(
    "lb.nightly.functions.rpc.get",
    return_value=Slot(
        "test-slot", build_id=123, projects=[Project("Moore", "HEAD")]
    ).Moore,
)
@patch("lb.nightly.db.connect")
@patch("lb.nightly.functions.checkout.git")
@patch("lb.nightly.functions.checkout.notify_gitlab")
@patch("lb.nightly.functions.checkout.update_nightly_git_archive")
def test_checkout_full(
    update_nightly_git_archive,
    notify_gitlab,
    git,
    connect,
    get,
    service_config,
    tmp_path,
):
    git.side_effect = MockGit()
    update_nightly_git_archive().md.return_value = "md report"
    update_nightly_git_archive().to_dict.return_value = {}
    update_nightly_git_archive.reset_mock()

    with patch("json.dump"), working_directory(tmp_path):
        report = checkout("flavour/slot/123/project", "")

    service_config.assert_called_once()
    get.assert_called_once()
    connect.assert_called_once()
    git.assert_called_once()
    notify_gitlab.assert_called_once()
    update_nightly_git_archive.assert_called_once()

    archive_path = tmp_path / os.path.basename(get().artifacts("checkout"))
    assert zipfile.is_zipfile(archive_path)
    with zipfile.ZipFile(archive_path) as archive:
        names = archive.namelist()
        assert "Moore/data.txt" in names
        assert archive.read("Moore/data.txt").decode() == "data\n"
        assert "Moore/.logs/checkout/report.json" in names
        assert "Moore/.logs/checkout/report.md" in names

    artifact_path = tmp_path / "artifacts" / get().artifacts("checkout")
    assert os.path.exists(artifact_path)
    assert open(archive_path, "rb").read() == open(artifact_path, "rb").read()


@patch("lb.nightly.functions.rpc.service_config")
@patch(
    "lb.nightly.functions.rpc.get",
    return_value="something",
)
def test_checkout_bad_get_result(get, service_config):
    with pytest.raises(ValueError):
        checkout("flavour/slot/123/project", "")

    get.assert_called_once()


@patch(
    "lb.nightly.functions.rpc.service_config",
    return_value={
        "gitlab": {"token": None},
        "artifacts": {"uri": "artifacts"},
        "logs": {"uri": "logs"},
    },
)
@patch(
    "lb.nightly.functions.rpc.get",
    return_value=Slot(
        "test-slot", build_id=123, projects=[Project("Moore", "HEAD")]
    ).Moore,
)
@patch("lb.nightly.functions.rpc.db")
@patch("lb.nightly.functions.checkout.git")
@patch("lb.nightly.functions.checkout.notify_gitlab")
@patch("lb.nightly.functions.checkout.update_nightly_git_archive")
def test_checkout_no_gitlab_notify(
    update_nightly_git_archive,
    notify_gitlab,
    git,
    db,
    get,
    service_config,
    tmp_path,
):
    git.side_effect = MockGit()
    update_nightly_git_archive().md.return_value = "md report"
    update_nightly_git_archive().to_dict.return_value = {}
    update_nightly_git_archive.reset_mock()

    with patch("json.dump"), working_directory(tmp_path), patch.dict(
        os.environ, {"GITLAB_TOKEN": ""}
    ):
        report = checkout("flavour/slot/123/project", "")

    service_config.assert_called_once()
    get.assert_called_once()
    db.assert_has_calls([db(), db(), db()])
    git.assert_called_once()
    notify_gitlab.assert_not_called()
    update_nightly_git_archive.assert_called_once()

    archive_path = tmp_path / os.path.basename(get().artifacts("checkout"))
    assert zipfile.is_zipfile(archive_path)
    with zipfile.ZipFile(archive_path) as archive:
        names = archive.namelist()
        assert "Moore/data.txt" in names
        assert archive.read("Moore/data.txt").decode() == "data\n"
        assert "Moore/.logs/checkout/report.json" in names
        assert "Moore/.logs/checkout/report.md" in names

    artifact_path = tmp_path / "artifacts" / get().artifacts("checkout")
    assert os.path.exists(artifact_path)
    assert open(archive_path, "rb").read() == open(artifact_path, "rb").read()


@patch(
    "lb.nightly.functions.rpc.service_config",
    return_value={
        "gitlab": {"token": "some_token"},
        "artifacts": {"uri": "artifacts"},
        "logs": {"uri": "logs"},
    },
)
@patch(
    "lb.nightly.functions.rpc.get",
    return_value=Slot(
        "test-slot", build_id=123, projects=[Project("Moore", "HEAD")]
    ).Moore,
)
@patch("lb.nightly.functions.rpc.db")
@patch("lb.nightly.functions.checkout.git")
@patch("lb.nightly.functions.checkout.notify_gitlab")
@patch("lb.nightly.functions.checkout.update_nightly_git_archive")
def test_checkout_git_archive_fail(
    update_nightly_git_archive,
    notify_gitlab,
    git,
    db,
    get,
    service_config,
    tmp_path,
):
    git.side_effect = MockGit()
    notify_gitlab().md.return_value = "md report"
    notify_gitlab().to_dict.return_value = {}
    notify_gitlab.reset_mock()
    update_nightly_git_archive.side_effect = RuntimeError("some problem")

    with patch("json.dump"), working_directory(tmp_path):
        report = checkout("flavour/slot/123/project", "")

    service_config.assert_called_once()
    get.assert_called_once()
    db.assert_has_calls([db(), db(), db()])
    git.assert_called_once()
    notify_gitlab.assert_called_once()
    update_nightly_git_archive.assert_called_once()
    # we get warnings because the repositories for artifacts and logs are not configured
    notify_gitlab().warning.assert_called()

    archive_path = tmp_path / os.path.basename(get().artifacts("checkout"))
    assert zipfile.is_zipfile(archive_path)
    with zipfile.ZipFile(archive_path) as archive:
        names = archive.namelist()
        assert "Moore/data.txt" in names
        assert archive.read("Moore/data.txt").decode() == "data\n"
        assert "Moore/.logs/checkout/report.json" in names
        assert "Moore/.logs/checkout/report.md" in names

    artifact_path = tmp_path / "artifacts" / get().artifacts("checkout")
    assert os.path.exists(artifact_path)
    assert open(archive_path, "rb").read() == open(artifact_path, "rb").read()


@patch(
    "lb.nightly.functions.rpc.service_config",
    return_value={"gitlab": {"token": "some_token"}},
)
@patch(
    "lb.nightly.functions.rpc.get",
    return_value=Slot(
        "test-slot", build_id=123, projects=[Project("Moore", "HEAD")]
    ).Moore,
)
@patch("lb.nightly.functions.rpc.db")
@patch("lb.nightly.functions.checkout.git")
@patch("lb.nightly.functions.checkout.notify_gitlab")
@patch("lb.nightly.functions.checkout.update_nightly_git_archive")
def test_checkout_full(
    update_nightly_git_archive,
    notify_gitlab,
    git,
    db,
    get,
    service_config,
    tmp_path,
):
    git.side_effect = MockGit()
    update_nightly_git_archive().md.return_value = "md report"
    update_nightly_git_archive().to_dict.return_value = {}
    update_nightly_git_archive.reset_mock()

    with patch("json.dump"), working_directory(tmp_path):
        report = checkout("flavour/slot/123/project", "")

    service_config.assert_called_once()
    get.assert_called_once()
    db.assert_has_calls([db(), db(), db()])
    git.assert_called_once()
    notify_gitlab.assert_called_once()
    update_nightly_git_archive.assert_called_once()
    # we get warnings because the repositories for artifacts and logs are not configured
    update_nightly_git_archive().warning.assert_called()

    archive_path = tmp_path / os.path.basename(get().artifacts("checkout"))
    assert zipfile.is_zipfile(archive_path)
    with zipfile.ZipFile(archive_path) as archive:
        names = archive.namelist()
        assert "Moore/data.txt" in names
        assert archive.read("Moore/data.txt").decode() == "data\n"
        assert "Moore/.logs/checkout/report.json" in names
        assert "Moore/.logs/checkout/report.md" in names

    artifact_path = tmp_path / "artifacts" / get().artifacts("checkout")
    assert not os.path.exists(artifact_path)


@patch(
    "lb.nightly.functions.rpc.service_config",
    return_value={"gitlab": {"token": "some_token"}},
)
@patch(
    "lb.nightly.functions.rpc.get",
    return_value=Slot(
        "test-slot",
        build_id=123,
        projects=[DBASE(packages=[Package("PRConfig", "v2r0")])],
    ).DBASE,
)
@patch("lb.nightly.functions.rpc.db")
@patch("lb.nightly.functions.checkout.git")
@patch("lb.nightly.functions.checkout.notify_gitlab")
@patch("lb.nightly.functions.checkout.update_nightly_git_archive")
def test_checkout_full_data_project(
    update_nightly_git_archive,  # not yet used by data packages
    notify_gitlab,
    git,
    db,
    get,
    service_config,
    tmp_path,
):
    mock_report = Mock()
    mock_report.packages = [Mock()]
    mock_report.md.return_value = "md report"
    mock_report.to_dict.return_value = {}

    git.side_effect = MockGit(mock_report)

    with patch("json.dump"), working_directory(tmp_path):
        report = checkout("flavour/slot/123/project", "")

    service_config.assert_called_once()
    get.assert_called_once()
    db.assert_has_calls([db(), db(), db()])
    git.assert_called_once()
    notify_gitlab.assert_called_once()
    # not yet used by data packages
    # update_nightly_git_archive.assert_called_once()
    # # we get warnings because the repositories for artifacts and logs are not configured
    # update_nightly_git_archive().warning.assert_called()

    archive_path = tmp_path / os.path.basename(get().artifacts("checkout"))
    assert zipfile.is_zipfile(archive_path)
    with zipfile.ZipFile(archive_path) as archive:
        names = archive.namelist()
        assert "DBASE/data.txt" in names
        assert archive.read("DBASE/data.txt").decode() == "data\n"
        assert "DBASE/.logs/checkout/report.json" in names
        assert "DBASE/.logs/checkout/report.md" in names

    artifact_path = tmp_path / "artifacts" / get().artifacts("checkout")
    assert not os.path.exists(artifact_path)
